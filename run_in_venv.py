#!/usr/bin/env python
"""
Python installer for the SWAN environment.

Creates a virtual environment in a special directory and then sets the sys.path
to point to it.

"""
# Note: This script must be run inside Python so that it can control
#       things like the sys.path
#       In IPython you can use the %run magic for this purpose.

_locals_before = set(locals())

import argparse
import os
import site
import subprocess
import sys


def configure_parser(parser: argparse.ArgumentParser) -> None:
    parser.description = 'Install using pip into a local virtual environment'
    parser.add_argument('venv-name', help='The name of the venv to create')
    parser.add_argument(
        '--no-disable-user-site', action="store_true",
        help="Don't disable the user-site directory (normally at ~/.local)",
    )
    parser.add_argument(
        '--quiet', action="store_true",
        help="Don't print any output",
    )
    parser.add_argument(
        '--no-acc-pypi', action="store_true",
        help="Disable the acc-py-pip-config (i.e. don't use the Acc-Py package index)",
    )
    parser.add_argument(
        'exec-args', nargs=argparse.REMAINDER,
        help='Arguments to be passed on when invoking Python in the environment',
    )
 
 
def handler(args) -> None:
    """
    Execute the desired code **and** update the current Python to look like
    the desired environment. This allows subsequent in-process execution, such
    as being able to run this in a Jupyter context.

    """
    venv_name = getattr(args, 'venv-name')
    venv_location = create_venv(venv_name, quiet=args.quiet)
    extra_env_vars = {}
    
    # Remove the .local from sys.path. User site-packages are harmful and break
    # isolation.
    if not args.no_disable_user_site:
        dot_local_dir = site.getusersitepackages()
        for path in sys.path[:]:
            if path.startswith(dot_local_dir):
                sys.path.remove(path)
        # Since cmmnbuild-dep-manager >=2.6 the site.ENABLE_USER_SITE was
        # checked to determine if user site-packages should be added.
        site.ENABLE_USER_SITE = False

        extra_env_vars.update({'PYTHONNOUSERSITE': "1"})

    venv_site_packages = os.path.join(
        venv_location, 'lib', 'python{}.{}'.format(*sys.version_info), 'site-packages')
    
    # If the venv site-packages doesn't exist on the path already, insert it right
    # before the underlying Python's site-packages.
    if venv_site_packages not in sys.path:
        posn = -1
        site_package_dirs = [
            index for index, path in enumerate(sys.path) if path.endswith('site-packages')]
        if site_package_dirs:
            posn = site_package_dirs[0]
        sys.path.insert(posn, venv_site_packages)

    exec_args = list(getattr(args, 'exec-args'))

    if not args.no_acc_pypi:
        extra_env_vars.update({
            'PIP_INDEX_URL': 'https://acc-py-repo.cern.ch/repository/vr-py-releases/simple',
            'PIP_TRUSTED_HOST': 'acc-py-repo.cern.ch',
        })
    exec_in_venv(
        venv_location, exec_args, quiet=args.quiet,
        extra_env=extra_env_vars,
    )
    

def create_venv(name, quiet=False):
    install_loc = os.path.join(
        os.environ['HOME'], 'python', 'environments', name
    )
    # Only create the venv if it doesn't already exist.
    # Creating a venv is really slow on some filesystems (incl. EOS).
    if not os.path.exists(install_loc):
        cmd = [sys.executable, '-m', 'venv', install_loc, '--system-site-packages']
        run_w_stream(cmd)
        
        # Update the pip installation of the venv.
        env_py = os.path.join(install_loc, 'bin', 'python')
        cmd = [env_py, '-m', 'pip', 'install', '--upgrade', 'pip']
        run_w_stream(cmd, quiet=quiet, env={'PYTHONNOUSERSITE': "1"})

    return install_loc


def exec_in_venv(environment_prefix, exec_args, quiet=False, extra_env=None):
    """
    Execute the given Python arguments in the given environment prefix.

    """
    env_py = os.path.join(environment_prefix, 'bin', 'python')
    cmd = [env_py] + exec_args
    run_w_stream(cmd, env=extra_env, quiet=quiet)


def main():
    parser = argparse.ArgumentParser()
    configure_parser(parser)
    
    args = parser.parse_args()
    handler(args)
    

def run_w_stream(cmd, env=None, quiet=False):
    """
    Run the given command in a subprocess, but stream the output to stdout as it arrives.
    
    Note: Stderr is also piped to stdout for simplicity of implementation.
    
    """
    full_env = os.environ.copy()
    full_env.update(env or {})
    if not quiet:
        print('Running: ', ' '.join(cmd))
    proc = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
        env=full_env,
    )
    while not proc.poll():
        line = proc.stdout.readline()
        if line:
            if not quiet:
                print(line.decode('utf8'), end="")
        else:
            break
    # Capture any remaining stdout.
    for line in proc.stdout.readlines():
        if not quiet:
            print(line.decode('utf8'), end="")


main()


# Tidy up since this is being run in the active kernel environment
for local_before in set(locals()) - _locals_before:
    locals().pop(local_before)
locals().pop('_local_before', None)
