# SWAN Run in venv

A tool to run a Python command in a venv and have it affect the
currently running kernel. Perfect for installing packages without
having to install them into the ``$HOME/.local`` directory.


## Why not just use ``--user`` / ``$HOME/.local``?

When Python has been installed centrally (not by you), but you
want to install a package for use in the environment, you will often find
advice to add the ``--user`` flag to your ``pip install`` command.
This will install packages to your ``$HOME/.local`` directory, and these will
be put on the Python path by default.

The ``$HOME/.local`` directory has the behaviour that it is added to the Python
path for **all** Python environments - it has a global effect on all of your
future Python exections. Essentially this breaks the idea of
environment isolation and makes it very hard to create reproducible environments.

The general advice would be to *always* create a virtual environment and to
pip install into this environment. In this way you do not change the software
for all of your Python environments, and will have a much simpler time ensuring
reproducibility.


## What is special about SWAN?

In SWAN this is made complex by the fact that it is not easy to create a new
Jupyter kernel for your venv and to restart the notebook with this kernel selected.
Instead we must take the following steps:

 * Create a venv
 * Run ``pip install`` from the venv
 * Add the venv to the running kernel's ``sys.path``
 * Ensure that the ``.local`` directory isn't polluting our environment


## How to use in SWAN?

Because of the IPython magics that are available to us in the Jupyter / SWAN environment,
we can fetch the installer and execute it in the running kernel in 2 simple commands. For example,
to install flake8 in a clean, non-global way, using a virtual environment:

```
!curl -s https://gitlab.cern.ch/pelson/swan-run-in-venv/-/raw/master/run_in_venv.py -o .run_in_venv
%run .run_in_venv my-venv -m pip install --upgrade flake8
```

Note that the first time you run this command it will create a virtual environment called "my-venv"
in ``$HOME/python/environments/my-venv``. Depending on the filesystem this can be quite slow.
Subsequent calls in this or other notebooks will be able to re-use this named virtual environment
and will be much quicker.

